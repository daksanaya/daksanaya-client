import { Text } from "@chakra-ui/layout";
import { useRouter } from "next/router";
import Services from "../../Components/Container/Akademi/Services";
import { useState, useEffect } from "react";

export default function DaksaService() {


    const router = useRouter()
    const service = router.query.services

    if (service == undefined) {
        return (
            <Text pt="500px">Loading ...</Text>
        )
    } else {
        return (
            <Services service={service} />

        )
    }

}
