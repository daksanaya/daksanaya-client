import { ChakraProvider } from '@chakra-ui/react';
import Head from 'next/head';
import theme from '../styles/ChakraConf';
import '../styles/globals.css';
import Layout from '../Components/Layout';

function MyApp({ Component, pageProps }) {
  return (
    <ChakraProvider theme={theme}>
      <Head>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta
          name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"
        />
        <meta
          name="description"
          content="Daksanaya Manajemen adalah sebuah perusahaan konsultasi keuangan dan investasi"
        />
        <meta name="keywords" content="Keywords" />
        <title>Daksanaya</title>

        <link rel="manifest" href="/manifest.json" />
        <link
          href="/images/daksanaya-bundar.png"
          rel="icon"
          type="image/png"
          sizes="50x25"
        />
        <link
          href="/icons/favicon-32x32.ico"
          rel="icon"
          type="image/png"
          sizes="32x32"
        />
        <link rel="apple-touch-icon" href="/icons/apple-icon.ico"></link>
        <meta name="theme-color" content="#003D86" />
      </Head>
      <Layout>
        <Component {...pageProps} />
      </Layout>



    </ChakraProvider>
  );
}

export default MyApp;
