import { Box } from "@chakra-ui/layout";
import WhiteCard from "./WhiteCard";

const RegistrationCard = ({ children, ...props }) => {
    return (
        <Box
            boxShadow="5px 5px 5px rgba(55, 84, 170, 0.19), -5px -5px 10px rgba(20, 32, 112, 0.19)"
            bgColor="white"
            borderRadius="30px"
            {...props}
        >
            {children}

        </Box >
    );
}

export default RegistrationCard;