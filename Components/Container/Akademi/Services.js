import { ServiceWrapper } from "./Style";
import { Box, Flex, Text } from "@chakra-ui/layout";
import SecondaryButton from '../../Button/SecondaryButton';
import MainTitle from "../../Title/MainTitle";
import WhiteCard from "../../Card/WhiteCard";
import { useState, useEffect } from "react";
import { Offer, Benefit } from './ServiceContent'
import { SERVICE_OPTIONS } from "./Constant";


const Services = ({ service, ...props }) => {
    const route = service;
    let serviceOption;
    switch (route) {
        case 'perencanaan-keuangan':
            serviceOption = 0;
            break;
        case 'keuangan-perusahaan':
            serviceOption = 1;
            break;
        case 'manajemen-investasi':
            serviceOption = 2;
            break;
        case 'analisis-laporan-keuangan':
            serviceOption = 3;
            break;
        case 'valuasi-saham':
            serviceOption = 4;
            break;
        default:
            serviceOption = 10
    }

    const [option, setOption] = useState();
    const [isLoaded, setIsLoaded] = useState(false);

    useEffect(() => {
        setOption(serviceOption), setIsLoaded(true)
    });
    console.log(isLoaded)
    console.log(route)
    console.log(option)

    if (isLoaded) {
        return (
            <>
                <Flex
                    flexDir="column"
                    justifyContent="center"
                    alignItems="center"
                >
                    <ServiceWrapper>
                        <Box
                            backgroundColor="rgba(0, 0, 0, 0.6)"
                            position="absolute"
                            top="0"
                            left="0"
                            width="100%"
                            height="100%"

                        >
                            <Flex
                                flexDir="column"
                                justifyContent="center"
                                alignItems="center"
                                pt="200px"
                            >

                                <MainTitle
                                    color="white"
                                >
                                    {SERVICE_OPTIONS[option][0]?.title}
                                </MainTitle>

                            </Flex>

                        </Box>
                    </ServiceWrapper>

                    <WhiteCard
                        mx="auto"
                        h={{ lg: "350px", base: "550px" }}
                        w={{ lg: "60vw", base: '90vw' }}

                        mb="30px"
                        mt="60px"
                        display="flex"
                        flexDir="column"

                        alignItems="center"

                    >

                        <Text
                            my="40px"
                            fontSize={{ lg: "2rem", base: "1.5rem" }}
                        >
                            {SERVICE_OPTIONS[option][0]?.title}
                        </Text>
                        <Text
                            fontSize={{ lg: "1rem", base: "1rem" }}
                            maxW={{ lg: "50vw", base: '80vw' }}
                        >
                            {SERVICE_OPTIONS[option][1]?.deskripsi}
                        </Text>

                    </WhiteCard>


                    <Flex
                        flexDir={{ lg: "row", base: 'column-reverse' }}
                        flexWrap="wrap"
                        justifyContent="center"
                        alignItems="center"
                        mb="50px"
                    >

                        <Offer
                            offerChildren={SERVICE_OPTIONS[option][2]?.offer}
                        />

                    </Flex>


                    <Flex
                        flexDir={{ lg: "row", base: 'column' }}
                        flexWrap="wrap"
                        justifyContent="center"
                        alignItems="center"
                    >

                        <Benefit
                            benefitChildren={SERVICE_OPTIONS[option][3]?.benefit}
                        />

                    </Flex>

                    <SecondaryButton
                        my="40px"
                    >
                        Daftar Sekarang
                    </SecondaryButton>

                </Flex>

            </>
        )
    } else {
        return (
            <Text pt="400px">Loading . . .</Text>
        )
    }


}

export default Services;