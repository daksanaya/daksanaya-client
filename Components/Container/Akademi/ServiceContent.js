
import { HStack, Text, VStack } from "@chakra-ui/layout";
import { Image } from "@chakra-ui/image";


export const Offer = ({ offerChildren }) => {

    return (
        <>
            <VStack
                w={{ lg: "450px" }}
                h={{ lg: "400px" }}
                spacing={5}
                align="left"
            >
                <Text
                    fontSize={{ lg: "2rem", base: "1.25rem" }}
                >
                    Apa saja materi yang diberikan ?
                </Text>

                {offerChildren.map((offer) => (
                    <HStack spacing={5}>
                        <Image
                            src="/images/icons/dot.svg"
                        />
                        <Text>
                            {offer}
                        </Text>
                    </HStack>

                ))}


            </VStack>
            <Image
                src="/images/akademi/services-img-1.svg"
                ml={{ lg: "30px", base: '0px' }}
                w={{ lg: "535px", base: '300px' }}
                h={{ lg: "535px", base: '300px' }}
            />
        </>
    )
}


export const Benefit = ({ benefitChildren }) => {
    return (
        <>
            <Image
                src="/images/akademi/services-img-2.svg"
                mr={{ lg: "30px", base: '0px' }}
                w={{ lg: "535px", base: '300px' }}
                h={{ lg: "535px", base: '300px' }}
            />
            <VStack
                w={{ lg: "500px" }}
                h={{ lg: "400px" }}
                spacing={10}
                align="left"
            >
                <Text
                    fontSize={{ lg: "2rem", base: "1.25rem" }}
                >
                    Benefit yang akan kamu dapatkan ?
                </Text>
                {benefitChildren.map((benefit) => (
                    <HStack spacing={5}>
                        <Image
                            src="/images/icons/acc.svg"
                        />
                        <Text>
                            {benefit}
                        </Text>
                    </HStack>
                ))}
            </VStack>
        </>
    )
}