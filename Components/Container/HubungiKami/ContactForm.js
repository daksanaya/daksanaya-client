import { Box, Grid, Text, GridItem } from "@chakra-ui/layout";
import { VStack, HStack, Input } from "@chakra-ui/react";
import PrimaryButton from '../../Button/PrimaryButton'
import { Image } from "@chakra-ui/image";
import {
    FormControl,
    FormLabel,
    FormErrorMessage,
    FormHelperText,
} from "@chakra-ui/react"

const ContactForm = ({ }) => {
    return (
        <>
            <Box
                w="80vw"
                h={{ lg: "500px", base: "800px" }}
                my="100px"
                mx="auto"
                borderRadius="50px"
                boxShadow="15px 15px 10px rgba(0, 0, 0, 0.1), -15px -15px 10px rgba(255, 255, 255, 0.1)"
                py="50px"
                display="flex"
                flexDir="row"
                flexWrap="wrap"
                justifyContent="center"
                alignItems="cennter"
            >
                <Box
                    display="flex"
                    flexDir="column"
                    justifyContent="center"
                    alignItems="left"
                    mx={{ lg: "40px" }}
                >
                    <HStack
                        my={{ lg: "30px", base: "15px" }}
                    >
                        <Image
                            src="/images/icons/map.png"
                            maxW="35px"
                            maxH="35px"
                            mr="30px"
                        />
                        <VStack align="left" maxW="250px">
                            <Text fontSize="1rem" fontWeight="medium">Lokasi :</Text>
                            <Text>Jl. Mampang Prapatan Raya No. 73 Jakarta Selatan</Text>

                        </VStack>
                    </HStack>

                    <HStack
                        my={{ lg: "30px", base: "15px" }}
                    >
                        <Image
                            src="/images/icons/gmail.png"
                            maxW="35px"
                            maxH="35px"
                            mr="30px"
                        />
                        <VStack align="left" maxW="250px">
                            <Text fontSize="1rem" fontWeight="medium">
                                Email :
                            </Text>
                            <Text>daksanayamanagement@gmail.com</Text>

                        </VStack>
                    </HStack>

                    <HStack
                        my={{ lg: "30px", base: "15px" }}
                    >
                        <Image
                            src="/images/icons/call.png"
                            maxW="35px"
                            maxH="35px"
                            mr="30px"
                        />
                        <VStack align="left" maxW="250px">
                            <Text fontSize="1rem" fontWeight="medium">
                                Telepon :
                            </Text>
                            <Text>+62 812-9909-8344</Text>
                        </VStack>
                    </HStack>
                </Box>
                <Box
                    display="flex"
                    flexDir="column"
                    justifyContent="center"
                    alignItems="left"
                    mx={{ lg: "40px" }}
                >
                    <Grid
                        templateColumns={{ base: 'repeat(1, 1fr)', md: 'repeat(2, 1fr)' }}
                        columnGap="14px"
                        rowGap={{ base: '.75rem', md: '0' }}
                        mb="0.75rem"
                        w={{ lg: "500px", base: '250px' }}
                    >
                        <FormControl>
                            <FormLabel >Nama :</FormLabel>
                            <Input text="Nama" />

                        </FormControl>
                        <FormControl>
                            <FormLabel>Alamat Email :</FormLabel>
                            <Input text="Email" />

                        </FormControl>
                    </Grid>
                    <FormControl>
                        <FormLabel>Isi Pesan :</FormLabel>
                        <Input
                            textarea
                            text="Pesan"
                            h="10rem"

                        />
                    </FormControl>
                    <Box
                        align="right"
                    >
                        <PrimaryButton

                            w="6rem"
                            h="2.25rem"
                            mt="1.25rem"
                            fontSize="1rem"
                        >
                            Kirim
                        </PrimaryButton>
                    </Box>
                </Box>
            </Box>

        </>
    )
}

export default ContactForm;