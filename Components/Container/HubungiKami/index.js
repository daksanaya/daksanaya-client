import MainTitle from "../../Title/MainTitle";
import { MainWrapper, SectionGap } from "./Style";
import { Box } from "@chakra-ui/layout";
import WhiteCard from "../../Card/WhiteCard";
import ContactForm from "./ContactForm";

const HubungiKami = ({ }) => {
    return (
        <>
            <MainWrapper>
                <Box
                    display="flex"
                    flexDir="column"
                    justifyContent="center"
                    alignItems="center"
                >
                    <MainTitle
                        color="white"
                    >
                        Hubungi Kami
                    </MainTitle>
                </Box>
            </MainWrapper>
            <ContactForm />


        </>
    )
}

export default HubungiKami;