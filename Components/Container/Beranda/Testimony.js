import React from 'react';
import { Box, Text, VStack, Image } from '@chakra-ui/react';

function Testimony() {
    return (
        <>
            <Box>
                <VStack w="100%" spacing="3vh"  >
                    <Image
                        borderRadius="full"
                        maxW={{ lg: "150px", base: '75px' }}
                        src="https://bit.ly/sage-adebayo"
                        alt="Segun Adebayo"
                    />
                    <Text fontSize={{ base: '1rem', lg: "2rem" }} fontWeight="600" color="white">
                        FARID FARITODY (MANAJER CABANG BNI)
                    </Text>
                    <Text color="white" fontSize={{ base: '1rem', lg: "1.5rem" }} fontWeight="400" maxW="75vw" lineHeight={{ lg: "3rem", base: '2rem' }} >
                        &quot;Sebagai Praktisi Keuangan, mengikuti event yang diselenggarakan Daksanaya memberikan tambahan informasi dan kesempatan upgrade isu-isu terkini baik di tataran teori dan praktek di market. Saya juga memberikan apresiasi kepada Daksanaya, ditengah pandemi yang terjadi memberikan terobosan pelaksanaan training Financial Education secara online dengan tidak mengurangi kualitas penyampaian materi dan juga didukung narasumber yang berpengalaman. Terimakasih, salam sehat.&quot;
                    </Text>
                    <Image
                        borderRadius="full"
                        maxH="121px"
                        maxW="91px"
                        src="/images/landing-quote.png"
                        alt="Segun Adebayo"
                        pb="8vh"
                    />

                </VStack>
            </Box>
        </>
    );
}

export default Testimony;
