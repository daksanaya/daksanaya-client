import WhiteCard from "../../Card/WhiteCard"
import { VStack, Center, Text } from "@chakra-ui/layout"
import { Image } from "@chakra-ui/image"
import PrimaryButton from "../../Button/PrimaryButton"

const ServiceCard = ({ title, description, imgprops }) => {
    console.log(imgprops)
    return (
        <WhiteCard
            h="320px"
            w="290px"
            display="flex"
            flexDirection="column"
            alignItems="flex-start"
            justifyContent="flex-start"
            gridGap="12px"
            mx={{ base: 'auto', lg: '30px' }}
            my={{ base: '20px', lg: "auto" }}
        >
            <Image
                src={imgprops}
                mt="5%"
                ml="5%"
            />
            <Center>
                <VStack spacing="8%">
                    <Text
                        align="center"
                        fontSize="1.5rem"
                        fontWeight="semibold"
                    >
                        {title}
                    </Text>
                    <Text
                        lineHeight="24px"
                        fontSize="1rem"
                        fontWeight="medium"
                        align="center"
                        maxW="80%"
                    >
                        {description}

                    </Text>
                    <PrimaryButton>
                        Selengkapnya
                    </PrimaryButton>
                </VStack>
            </Center>
        </WhiteCard>
    )
}
export default ServiceCard;