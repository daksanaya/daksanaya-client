import { Carousel as ReactCarousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css'; // requires a loader
import CarouselStyle from './Style'
import Testimony from './Testimony';
import { Box } from '@chakra-ui/layout';

const Carousel = ({ ...props }) => {

    return (
        <CarouselStyle>
            <Box
                {...props}
            >
                <ReactCarousel showThumbs={false} >
                    <Testimony />
                    <Testimony />
                    <Testimony />
                </ReactCarousel>
            </Box>

        </CarouselStyle>
    );

}

export default Carousel;
