import Head from "next/head";
import "semantic-ui-css/semantic.min.css";
import "react-multi-carousel/lib/styles.css";
import MainTitle from '../../Title/MainTitle'
import { SectionGap } from "./Style";
import { Flex } from "@chakra-ui/layout";
import ServiceCard from "./ServiceCard";
import Carousel from "./Carousel"
import SubTitle from "../../Title/SubTitle";
import MultiCarousel from "./MultiCarousel";
import {
    Box,
    Text,
    VStack,
} from '@chakra-ui/react';
import PrimaryButton from '../../Button/PrimaryButton'
import SectionHeader from "./SectionHeader";
import EventCard from "./EventCard";

const Beranda = ({ children, ...props }) => {
    return (
        <>
            <Flex
                flexDir="column"
                justifyContent="center"
                bgImage="url(/images/beranda/header.png)"
                backgroundSize="cover"
                backgroundRepeat="no-repeat"
                minW="100vw"
                minH="765px"
            >
                <VStack
                    maxW={{ base: '60vw', lg: '45vw' }}
                    spacing={{ base: 5, lg: 12 }}
                    align="left"
                    mx={{ base: 'auto', lg: '150px' }}
                >
                    <Text
                        fontSize={{
                            base: "2rem", lg: "3rem"
                        }}
                        color="white"
                        fontWeight="bold"

                    >
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.Maecenas eget pellentesque turpis.
                    </Text>
                    <Text
                        fontSize={{
                            base: "1rem", lg: "1.5rem"
                        }}
                        color="white"
                        fontWeight="normal"
                    >
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eget pellentesque turpis. Etiam at tortor lacinia, facilisis ex vitae, rhoncus leo.
                    </Text>
                    <PrimaryButton
                        maxW={{ base: '40vw', lg: "11vw" }}
                    >
                        Layanan Kami
                    </PrimaryButton>
                </VStack>
            </Flex>
            <SectionGap>
                <SubTitle
                    color="primary.darkBlueDaksa"
                    marginBottom="60px"
                >
                    Kenalin, Daksa. <br /> Si mentor keuangan kamu
                </SubTitle>
                <Box
                    display="flex"
                    flexWrap="wrap"
                    zIndex="1"
                >
                    <ServiceCard
                        imgprops="images/icons/book.png"
                        title="Akademi Daksa"
                        description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eget pellentesque turpis. Etiam at tortor lacinia, facilisis ex vitae, rhoncus leo. Sed id posuere eros."
                    />
                    <ServiceCard
                        imgprops="images/icons/paper.png"
                        title="Riset Daksa"
                        description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eget pellentesque turpis. Etiam at tortor lacinia, facilisis ex vitae, rhoncus leo. Sed id posuere eros."
                    />
                    <ServiceCard
                        imgprops="images/icons/chat.png"
                        title="Konsultasi Daksa"
                        description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eget pellentesque turpis. Etiam at tortor lacinia, facilisis ex vitae, rhoncus leo. Sed id posuere eros."
                    />
                </Box>
                <Box
                    bgColor="primary.blueDaksaBg"
                    w='100%'
                    h={{ base: '700px', lg: "960px" }}
                    mt={{ base: "1rem", lg: "-11rem" }}
                    borderRadius={{ lg: "150px 150px 0px 0px", base: '75px 75px 0px 0px' }}
                >
                    <SubTitle
                        color="white"
                        pt={{ base: '3rem', lg: '15rem' }}
                        lineHeight="72px"
                    >
                        Kata mereka tentang Daksa . . .
                    </SubTitle>
                    <Carousel
                        my={{ lg: "3vh", base: '1.5vh' }}
                    />
                </Box>
                <Box
                    bgColor="primary.blackDaksa"
                    w="100%"
                    h={{ lg: "1100px", base: '1800px' }}
                >
                    <SectionHeader>
                        <MainTitle
                            color="white"
                            mx={{ lg: '10vw', base: 'auto' }}
                        >
                            Gapai <Text as="i">finanncial freedom</Text><br /> bersama kami
                        </MainTitle>
                    </SectionHeader>
                    <EventCard />
                </Box>
                <Box
                    bgColor="white"
                    h={{ lg: "750px", base: '550px' }}
                    w="100vw"
                    borderRadius={{ lg: "150px 150px 0px 0px", base: '75px 75px 0px 0px' }}
                    mt={{ base: "-4rem", lg: "-8rem" }}
                    alignItems="center"
                >
                    <SubTitle
                        color="primary.darkBlueDaksa"
                        py={{ base: '3rem', lg: '6rem' }}
                        lineHeight="72px"
                    >
                        Belajar bareng daksa itu seru karena . . .
                    </SubTitle>
                    <MultiCarousel />
                </Box>
            </SectionGap>
        </>
    )
}

export default Beranda;