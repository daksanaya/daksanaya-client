import { Box } from "@chakra-ui/layout";

const SectionHeader = ({ children, ...props }) => {
    return (
        <Box
            display="flex"
            flexDirection="column"
            justifyContent="center"
            bgImage="/images/beranda/header.png"
            bgSize="cover"
            bgRepeat="norepeat"
            minW="100vw"
            minH={{ lg: '250px', base: '150px' }}
            borderRadius={{ lg: "0px 0px 150px 150px", base: '0px 0px 50px 50px' }}
            {...props}
        >
            {children}
        </Box >
    );
}

export default SectionHeader;