import PrimaryButton from "../../Button/PrimaryButton";
import { BlackCard } from "../../Card/BlackCard";
import { Box, Center, HStack, VStack, Text } from "@chakra-ui/layout";
import { Image } from "@chakra-ui/image";


const EventCard = ({ children, ...props }) => {
    return (
        <Box
            display="flex"
            flexDir={{ base: 'column', lg: 'row' }}
            flexWrap='wrap'
            alignItems="center"
            justifyContent="center"
            my="10vh"
        >
            <BlackCard
                w={{ lg: '350px', base: '275px' }}
                h={{ lg: "520px", base: '440px' }}
                display="flex"
                flexDir="column"
                alignItems="center"
                justifyContent="center"
                mx={{ lg: "30px", base: '0px' }}
                my={{ lg: "0px", base: '30px' }}
            >
                <Box
                    bgColor="primary.grayDaksa"
                    w={{ base: "250px", lg: '300px' }}
                    h={{ base: "250px", lg: '300px' }}
                    borderRadius="15px"
                    mb="15px"
                />
                <Text
                    align="center"
                    color="white"
                    fontSize={{ lg: "1.25rem", base: '1rem' }}
                    fontWeight="bold"
                    mb="15px"
                >
                    Value Investing Ala Warren Buffet
                </Text>
                <Box
                    alignItems="left"
                >
                    <HStack spacing="10px" align="left" mb="10px">
                        <Image src="/images/icons/calendar-mini.png" />
                        <Text color="white" fontSize={{ lg: "1rem", base: '0.75rem' }} fontWeight="light">16-17 dan 23-24 Agustus 2021</Text>
                    </HStack>
                    <HStack spacing="10px" align="left" mb="15px">
                        <Image src="/images/icons/clock-mini.png" />
                        <Text color="white" fontSize={{ lg: "1rem", base: '0.75rem' }} fontWeight="light">18.30 - 20.30 WIB</Text>
                    </HStack>
                </Box>
                <PrimaryButton maxW={{ lg: "40vw", base: '30vw' }}>
                    Selengkapnya
                </PrimaryButton>
            </BlackCard>
            <BlackCard
                w={{ lg: '350px', base: '275px' }}
                h={{ lg: "520px", base: '440px' }}
                display="flex"
                flexDir="column"
                alignItems="center"
                justifyContent="center"
                mx={{ lg: "30px", base: '0px' }}
                my={{ lg: "0px", base: '30px' }}
            >
                <Box
                    bgColor="primary.grayDaksa"
                    w={{ base: "250px", lg: '300px' }}
                    h={{ base: "250px", lg: '300px' }}
                    borderRadius="15px"
                    mb="15px"
                />
                <Text
                    align="center"
                    color="white"
                    fontSize={{ lg: "1.25rem", base: '1rem' }}
                    fontWeight="bold"
                    mb="15px"
                >
                    Value Investing Ala Warren Buffet
                </Text>
                <Box
                    alignItems="left"
                >
                    <HStack spacing="10px" align="left" mb="10px">
                        <Image src="/images/icons/calendar-mini.png" />
                        <Text color="white" fontSize={{ lg: "1rem", base: '0.75rem' }} fontWeight="light">16-17 dan 23-24 Agustus 2021</Text>
                    </HStack>
                    <HStack spacing="10px" align="left" mb="15px">
                        <Image src="/images/icons/clock-mini.png" />
                        <Text color="white" fontSize={{ lg: "1rem", base: '0.75rem' }} fontWeight="light">18.30 - 20.30 WIB</Text>
                    </HStack>
                </Box>
                <PrimaryButton maxW={{ lg: "40vw", base: '30vw' }}>
                    Selengkapnya
                </PrimaryButton>
            </BlackCard>
            <BlackCard
                w={{ lg: '350px', base: '275px' }}
                h={{ lg: "520px", base: '440px' }}
                display="flex"
                flexDir="column"
                alignItems="center"
                justifyContent="center"
                mx={{ lg: "30px", base: '0px' }}
                my={{ lg: "0px", base: '30px' }}
            >
                <Box
                    bgColor="primary.grayDaksa"
                    w={{ base: "250px", lg: '300px' }}
                    h={{ base: "250px", lg: '300px' }}
                    borderRadius="15px"
                    mb="15px"
                />
                <Text
                    align="center"
                    color="white"
                    fontSize={{ lg: "1.25rem", base: '1rem' }}
                    fontWeight="bold"
                    mb="15px"
                >
                    Value Investing Ala Warren Buffet
                </Text>
                <Box
                    alignItems="left"
                >
                    <HStack spacing="10px" align="left" mb="10px">
                        <Image src="/images/icons/calendar-mini.png" />
                        <Text color="white" fontSize={{ lg: "1rem", base: '0.75rem' }} fontWeight="light">16-17 dan 23-24 Agustus 2021</Text>
                    </HStack>
                    <HStack spacing="10px" align="left" mb="15px">
                        <Image src="/images/icons/clock-mini.png" />
                        <Text color="white" fontSize={{ lg: "1rem", base: '0.75rem' }} fontWeight="light">18.30 - 20.30 WIB</Text>
                    </HStack>
                </Box>
                <PrimaryButton maxW={{ lg: "40vw", base: '30vw' }}>
                    Selengkapnya
                </PrimaryButton>
            </BlackCard>


        </Box >


    );
}

export default EventCard;