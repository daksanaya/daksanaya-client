import { VStack, Text } from "@chakra-ui/layout";
import { Button, HStack, Image } from "@chakra-ui/react";
import WhiteCard from '../../Card/WhiteCard'
import SecondaryButton from "../../Button/SecondaryButton";


const BookContainer = ({ children, imgSrc, title, author, desc, ...props }) => {
    return (
        <WhiteCard
            flexDir={{ lg: "row", base: "column" }}
            flexWrap="wrap"
            w={{ lg: '900px', base: '400px' }}
            h={{ lg: "500px", base: '850px' }}
            alignItems="center"
            justifyContent="center"
            my="40px"
            {...props}
        >
            <Image
                src={imgSrc}
                h={{ lg: '400px', base: '375px' }}
                w={{ lg: '350px', base: '300px' }}
                borderRadius="40px"
                mx="20px"
            />
            <VStack spacing={3} mx={{ lg: "20px" }} align={{ base: 'center', lg: "left" }} my={{ base: '30px' }}>
                <Text
                    fontSize="1.25rem"
                    fontWeight="semibold"
                    maxW={{ lg: "400px" }}
                    align={{ base: "center", lg: 'left' }}

                >
                    {title}
                </Text>
                <HStack spacing={3}  >
                    <Image
                        src="/images/icons/pencil.svg"
                        w="15px"
                        h="15px"
                    />
                    <Text
                        fontSize={{ lg: "1rem" }}
                        fontWeight="medium"
                        color="primary.grayDaksa"

                    >
                        {author}
                    </Text>
                </HStack>
                <Text
                    fontSize="1rem"
                    fontWeight="normal"
                    maxW={{ lg: "350px", base: '350px' }}
                    align={{ base: "center" }}
                    pt="20px"
                    pb="20px"

                >
                    {desc}

                </Text>
                <SecondaryButton
                    w="150px"
                    fontSize="1rem"
                    mt="40px"
                >
                    Pesan Sekarang
                </SecondaryButton>




            </VStack>



        </WhiteCard >
    )
}

export default BookContainer;