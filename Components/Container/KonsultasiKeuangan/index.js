import { Flex, Box, VStack, HStack, Text } from "@chakra-ui/layout";
import { Image } from "@chakra-ui/image";
import MainTitle from "../../Title/MainTitle";
import WhiteCard from "../../Card/WhiteCard";
import PrimaryButton from "../../Button/PrimaryButton";
import {
    FormControl,
    FormLabel,
    FormErrorMessage,
    FormHelperText,
} from "@chakra-ui/react"
import { Input } from "@chakra-ui/input";

const KonsultasiKeuangan = ({ }) => {
    return (
        <>
            <Flex
                flexDir="column"
                justifyContent="center"
                bgImage="url(/images/konsultasi-keuangan/header.jpeg)"
                backgroundSize="cover"
                backgroundRepeat="no-repeat"
                minW="100vw"
                minH="500px"
                position="relative"
            >
                <Box
                    backgroundColor="rgba(0, 0, 0, 0.6)"
                    position="absolute"
                    top="0"
                    left="0"
                    width="100%"
                    height="100%"
                >
                    <Box
                        display="flex"
                        flexDir="column"
                        justifyContent="center"
                        alignItems="center"
                        pt={{ lg: "200px", base: '300px' }}
                    >
                        <MainTitle
                            color="white"
                        >
                            Konsultasi Keuangan
                        </MainTitle>

                    </Box>
                </Box>
            </Flex >
            <WhiteCard
                mx="auto"
                h={{ lg: "300px", base: "350px" }}
                w={{ lg: "60vw", base: '90vw' }}
                border="1px solid #C4C4C4"
                mb="30px"
                mt="60px"
            >
                <VStack mx="auto" spacing="40px" maxW={{ lg: "50vw", base: '75vw' }}>
                    <Text
                        pt="30px"
                        fontSize={{ lg: "2rem", base: "1.5rem" }}
                    >
                        Deskripsi Kegiatan
                    </Text>
                    <Text
                        fontSize={{ lg: "1rem", base: "1rem" }}
                        align="center"
                    >
                        Memberikan konsultasi atau saran profesional (profesional advise) berdasarkan pada kesepakatan bersama dengan klien. Memberikan review dan komentar terhadap rencana bisnis buatan klien dengan mengembangkan temuan, kesimpulan, dan rekomendasi untuk dipertimbangkan dan diputuskan oleh klien dalam proses perencanaan strategik.
                    </Text>
                </VStack>


            </WhiteCard>

            <Flex
                flexDir={{ lg: 'row', base: 'column-reverse' }}
                flexWrap="wrap"
                justifyContent="center"
                alignItems="center"
                w="100vw"
                my={{ lg: '60px', base: '40px' }}
                mx="auto"

            >
                <Flex
                    flexDir='column'
                    flexWrap="wrap"
                    w={{ lg: "550px", base: "350px" }}
                    my={{ lg: '60px', base: '40px' }}

                >
                    <Text
                        fontSize={{ lg: '1.5rem', base: '1.2rem' }}
                        fontWeight="semibold"
                        maxW="90vw"
                        my={{ lg: "10px" }}
                    >
                        Apakah anda ingin berkomunikasi dan berkonsultasi lebih lanjut bersama kami ?
                    </Text>
                    <Text
                        my={{ lg: "10px", base: '20px' }}
                    >
                        Lengkapi data diri Anda dan kami akan segera menghubungi Anda.
                    </Text>
                    <VStack spacing={8}>
                        <FormControl id="name" isRequired>
                            <FormLabel>Nama Anda</FormLabel>
                            <Input placeholder="Contoh : Wahib Adiyatma" />
                        </FormControl>
                        <FormControl id="contact">
                            <FormLabel>Nomor Telepon</FormLabel>
                            <Input placeholder="Contoh : 081227363388" />
                        </FormControl>
                        <FormControl id="help">
                            <FormLabel>Bantuan Yang Diperlukan</FormLabel>
                            <Input placeholder="Contoh : Konsultasi untuk dana pensiun" />
                        </FormControl>
                        <PrimaryButton
                            mt={4}
                            type="submit"
                            maxW={{ lg: "10vw", base: '100vw' }}
                            ml={{ lg: "33vw" }}
                        >
                            Kirim
                        </PrimaryButton>
                    </VStack>

                </Flex>
                <Image
                    src="/images/konsultasi-keuangan/consulting-img-1.svg"
                    ml={{ lg: "50px", base: '0px' }}
                    w={{ lg: "500px", base: '300px' }}
                    h={{ lg: "500px", base: '300px' }}
                />
            </Flex>
        </>
    )
}

export default KonsultasiKeuangan;