import { Button } from '@chakra-ui/button';

const PrimaryButton = ({ children, ...props }) => (
    <Button
        bg="primary.blueDaksa"
        borderRadius="10px"
        fontSize={{ lg: '1.25rem', base: '1rem' }}
        color="white"
        fontWeight={700}
        _focus={{ outline: 'none' }}
        _hover={{
            transform: "scale(1.05)",
        }}
        _active={{ bg: 'primary.blueDaksa' }}
        {...props}
    >
        {children}
    </Button>
);
export default PrimaryButton;